package com.itau.saqueinternacional.models;

import java.util.HashMap;

public class RatesResponse {
	
	private String base;
	private int timestamp;
	private String date;
	private HashMap rates;
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public int getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public HashMap getRates() {
		return rates;
	}
	public void setRates(HashMap rates) {
		this.rates = rates;
	}
}
