package com.itau.saqueinternacional.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.saqueinternacional.models.Customer;
import com.itau.saqueinternacional.models.WithDrawRequest;
import com.itau.saqueinternacional.repositories.CustomerRepository;
import com.itau.saqueinternacional.services.RatesConverter;

@RestController
public class CustomerController {
	
	@Autowired
	RatesConverter ratesConverter;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@RequestMapping(method=RequestMethod.GET, path="/saque")
	public ResponseEntity<Customer> saque(WithDrawRequest withDrawRequest)
	{
		double valor = ratesConverter.getCotacao(withDrawRequest.getCurrency(), withDrawRequest.getAmount());
		
		Optional<Customer> customer = customerRepository.findById(withDrawRequest.getClientid());
		double balance = customer.get().getBalance();
		customer.get().setBalance(balance - valor);
		
		Customer customerSalvo =customer.get();
		
		return ResponseEntity.ok().body(customerSalvo);
		
	}
}
