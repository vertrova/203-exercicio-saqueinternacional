package com.itau.saqueinternacional.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.saqueinternacional.models.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
	
	public Customer findByUserName(String userName);
}
