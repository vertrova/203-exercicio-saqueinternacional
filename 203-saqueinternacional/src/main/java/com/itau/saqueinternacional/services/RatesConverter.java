package com.itau.saqueinternacional.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.saqueinternacional.models.RatesResponse;

@Service
public class RatesConverter {
	
	RatesResponse rateResponse;
	
private RestTemplate restTemplate = new RestTemplate();
	
	public  double getCotacao(String to, double amount) {
		
		String url = "https://data.fixer.io/api/latest?access_key=e539e11f4de63138df2cea4a75a4ad58&symbols=" + to;
		
		rateResponse = restTemplate.getForObject(url, RatesResponse.class);
		
		return amount * Double.parseDouble(rateResponse.getRates().get(to).toString());

	}

}
